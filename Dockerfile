################### Build Image ###################
FROM maven:3-amazoncorretto-17 as build
COPY . .
RUN mvn clean install


################### Running Image ###################
FROM amazoncorretto:17
EXPOSE 8081
WORKDIR /workspace/app
COPY --from=build /target/*.jar booking-app.jar
ENTRYPOINT ["java","-jar","booking-app.jar"]