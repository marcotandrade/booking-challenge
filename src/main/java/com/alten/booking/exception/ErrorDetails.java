package com.alten.booking.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorDetails {

    private Long timestamp;
    private Integer status;
    private String error;
    private String message;
    private String path;

}
