package com.alten.booking.exception;

import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ResourceExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceExceptionHandler.class);

    @Value("${maximum.stay}")
    private Integer maxStay;

    @ExceptionHandler(BookingException.class)
    public ResponseEntity bookingExceptionHandler(BookingException ex, HttpServletRequest request) {
        LOGGER.error("bookingExceptionHandler: " + ex.getMessage());
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(ErrorDetails.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(ex.getMessage())
                        .error("Booking error found!")
                        .path(request.getRequestURI())
                        .build()
                );
    }
}