package com.alten.booking.entity;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;


import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "reservation_code")
    private UUID reservationCode;

    @ElementCollection
    @CollectionTable(name = "reservation_dates", joinColumns = @JoinColumn(name = "reservation_id"))
    @Column(name = "dates")
    private Set<LocalDate> reservationDates;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private Room room;

    public Reservation() {
    }

    @Builder
    public Reservation(Long id, UUID reservationCode, Set<LocalDate> reservationDates, Room room) {
        this.id = id;
        this.reservationCode = reservationCode;
        this.reservationDates = reservationDates;
        this.room = room;
    }
}
