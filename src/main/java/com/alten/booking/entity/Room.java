package com.alten.booking.entity;


import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;



@Data
@Entity
@Table(name = "room")
@ToString
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "floor")
    private Integer floor;

    @Column(name = "number")
    private Integer number;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    @ToString.Exclude
    private Hotel hotel;

}
