package com.alten.booking.repository;

import com.alten.booking.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoomRepository extends JpaRepository<Room, Integer> {

    Optional<Room> findByNumberAndFloor(Integer number, Integer floor);
}
