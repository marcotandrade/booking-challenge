package com.alten.booking.repository;

import com.alten.booking.entity.Reservation;
import com.alten.booking.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    Boolean existsByRoomAndReservationDatesIn(Room room, Set<LocalDate> dates);

    Reservation findByReservationCode(UUID code);

    int deleteByRoomAndReservationCode(Room room, UUID code);

}
