package com.alten.booking.dto.response;

import lombok.*;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReservationResponse {
    UUID id;
    Set<LocalDate> dates;
    Integer roomNumber;
    Integer floor;
}
