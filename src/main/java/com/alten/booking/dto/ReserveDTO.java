package com.alten.booking.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;


@Data
@Builder
@EqualsAndHashCode
public class ReserveDTO {

    UUID id;
    @NotNull(message = "The days for booking must be informed")
    Set<LocalDate> dates;
    Integer roomNumber;
    Integer floor;


}
