package com.alten.booking.dto.resquest;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReservationUpdateRequest {

    @NotNull(message = "The ID of booked dates must be informed")
    UUID id;
    @NotNull(message = "The days for booking must be informed")
    Set<LocalDate> dates;
    Integer roomNumber;
    Integer floor;
}
