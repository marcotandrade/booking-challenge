package com.alten.booking.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DefaultResponseDTO {

    private String message;
}
