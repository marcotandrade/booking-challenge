package com.alten.booking.resource;

import com.alten.booking.dto.DefaultResponseDTO;
import com.alten.booking.dto.ReserveDTO;
import com.alten.booking.dto.response.ReservationResponse;
import com.alten.booking.dto.resquest.ReservationRequest;
import com.alten.booking.dto.resquest.ReservationUpdateRequest;
import com.alten.booking.service.ReservationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.util.Set;

@Service
@RequestMapping("/reservation")
@RequiredArgsConstructor
public class ReservationResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationResource.class);

    private final ReservationService reservationService;

    @GetMapping("/available")
    public ResponseEntity<Set<LocalDate>> checkAvailability() {
        LOGGER.info("checkAvailability");
        return ResponseEntity.ok(reservationService.checkAvailableDates());
    }

    @GetMapping("/booked")
    public ResponseEntity<Set<LocalDate>> booked() {
        return ResponseEntity.ok(reservationService.getBookingInfo());
    }

    @PostMapping
    public ResponseEntity<ReservationResponse> booking(@Valid @RequestBody ReservationRequest reservationRequest) {
        return ResponseEntity.ok(reservationService.bookARoom(reservationRequest));
    }

    @DeleteMapping
    public ResponseEntity<DefaultResponseDTO> deleteBooking(@Valid @RequestBody ReserveDTO reserveDTO) {
        return ResponseEntity.ok(reservationService.deleteABooking(reserveDTO));
    }

    @PutMapping
    public ResponseEntity<ReserveDTO> updateBooking(@Valid @RequestBody ReservationUpdateRequest reservationUpdateRequest) {
        return ResponseEntity.ok(reservationService.updateBooking(reservationUpdateRequest));
    }

}
