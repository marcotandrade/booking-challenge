package com.alten.booking.service.impl;

import com.alten.booking.dto.DefaultResponseDTO;
import com.alten.booking.dto.ReserveDTO;
import com.alten.booking.dto.response.ReservationResponse;
import com.alten.booking.dto.resquest.ReservationRequest;
import com.alten.booking.dto.resquest.ReservationUpdateRequest;
import com.alten.booking.entity.Reservation;
import com.alten.booking.entity.Room;
import com.alten.booking.exception.*;
import com.alten.booking.repository.ReservationRepository;
import com.alten.booking.repository.RoomRepository;
import com.alten.booking.service.ReservationService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ReservationServiceImpl implements ReservationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationServiceImpl.class);

    private final ReservationRepository repository;
    private final RoomRepository roomRepository;

    @Value("${default.room.number}")
    private Integer defaultRoomNumber;

    @Value("${default.room.floor}")
    private Integer defaultRoomFloor;

    @Value("${maximum.stay}")
    private Integer maxStay;

    @Value("${maximum.reservation.days.advance}")
    private Integer maxDaysInAdvance;

    @Override
    public Set<LocalDate> checkAvailableDates() {

        Set<LocalDate> res = new HashSet<>();
        LocalDate today = LocalDate.now();
        for (int i = 0; i < maxDaysInAdvance; i++) {
            res.add(today);
            today = today.plusDays(1);
        }
        res.removeAll(bookedDates());
        LOGGER.info("All availabilities: {}",res);
        return res;
    }

    @Override
    public ReservationResponse bookARoom(ReservationRequest  reservationRequest) {
        ReserveDTO reserveDTO = ReserveDTO.builder().dates(reservationRequest.getDates()).build();
        Room room = validationRules(reserveDTO, false);
        UUID uuid = UUID.randomUUID();
        Reservation reservation = Reservation.builder().reservationCode(uuid).reservationDates(reserveDTO.getDates()).room(room).build();
        Reservation persistedReservation = repository.save(reservation);
        return ReservationResponse.builder().id(persistedReservation.getReservationCode())
                .floor(persistedReservation.getRoom().getFloor()).roomNumber(persistedReservation.getRoom().getNumber())
                .dates(persistedReservation.getReservationDates()).build();
    }

    @Transactional(Transactional.TxType.REQUIRED)
    @Override
    public DefaultResponseDTO deleteABooking(ReserveDTO reserveDTO) {
        Room room = validationRules(reserveDTO, true);
        int deletedRecords = repository.deleteByRoomAndReservationCode(room, reserveDTO.getId());
        return DefaultResponseDTO.builder().message(deletedRecords + " record(s) deleted").build();
    }

    @Override
    public ReserveDTO updateBooking(ReservationUpdateRequest reservationUpdateRequest) {
        ReserveDTO  reserveDTO = ReserveDTO.builder().id(reservationUpdateRequest.getId())
                .dates(reservationUpdateRequest.getDates())
                .floor(reservationUpdateRequest.getFloor())
                .roomNumber(reservationUpdateRequest.getRoomNumber())
                .build();
        validationRules(reserveDTO, false);
        Reservation reservation = repository.findByReservationCode(reserveDTO.getId());
        reservation.setReservationDates(reserveDTO.getDates());
        Reservation updatedReservation = repository.save(reservation);
        return ReserveDTO.builder().id(updatedReservation.getReservationCode())
                .floor(updatedReservation.getRoom().getFloor()).roomNumber(updatedReservation.getRoom().getNumber())
                .dates(updatedReservation.getReservationDates()).build();
    }


    @Override
    public Set<LocalDate> getBookingInfo() {
        return bookedDates();
    }

    private Room validationRules(ReserveDTO reserveDTO, boolean isDeletion) {
        checkStayLimit(reserveDTO, isDeletion);
        Room room = findRoom(reserveDTO);
        checkAvailability(room, reserveDTO.getDates(), isDeletion);
        return room;
    }

    private void checkStayLimit(ReserveDTO reserveDTO, boolean isDeletion) {
        checkValidDates(reserveDTO.getDates(), isDeletion);
        if (reserveDTO.getDates().size() > maxStay) {
            throw new BookingException("The maximum stay is " + maxStay + " days");
        }
    }

    private void checkValidDates(Set<LocalDate> dates, boolean isDeletion) {
        dates.stream().filter(localDate -> localDate.isBefore(LocalDate.now())).findAny()
                .ifPresent(e -> {
                    throw new BookingException("Attention, you selected a past date for booking!");
                });

        dates.stream().filter(localDate -> localDate.isAfter(LocalDate.now().plusDays(maxDaysInAdvance))).findAny()
                .ifPresent(e -> {
                    throw new BookingException("Attention, you can select date with only "
                            + maxDaysInAdvance + " days in advance");
                });
        if (isDeletion) {
            dates.stream().filter(localDate -> localDate.isEqual(LocalDate.now())).findAny()
                    .ifPresent(e -> {
                        throw new BookingException("Attention, you can not delete a booking that already started!");
                    });
        }

    }

    private void checkAvailability(Room room, Set<LocalDate> dates, boolean isDeletion) {
        if (!isDeletion && repository.existsByRoomAndReservationDatesIn(room, dates)) {
            throw new BookingException("The room " + room.getNumber() + " is already reserved");
        }
    }

    private Room findRoom(ReserveDTO reserveDTO) {
        Room room = null;
        if (null == reserveDTO.getRoomNumber()) {
            room = roomRepository.findByNumberAndFloor(defaultRoomNumber, defaultRoomFloor)
                    .orElseThrow(BookingException::new);
        } else {
            room = roomRepository.findByNumberAndFloor(reserveDTO.getRoomNumber(), reserveDTO.getFloor())
                    .orElseThrow(BookingException::new);
        }
        return room;
    }


    private Set<LocalDate> bookedDates() {
        Set<LocalDate> bookedDates = new HashSet<>();
        repository.findAll().forEach(reservation -> bookedDates.addAll(reservation.getReservationDates()));
        return bookedDates;
    }


}
