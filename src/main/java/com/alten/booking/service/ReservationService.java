package com.alten.booking.service;

import com.alten.booking.dto.DefaultResponseDTO;
import com.alten.booking.dto.ReserveDTO;
import com.alten.booking.dto.response.ReservationResponse;
import com.alten.booking.dto.resquest.ReservationRequest;
import com.alten.booking.dto.resquest.ReservationUpdateRequest;

import java.time.LocalDate;
import java.util.Set;

public interface ReservationService {

    Set<LocalDate> checkAvailableDates();

    ReservationResponse bookARoom(ReservationRequest reservationRequest);

    ReserveDTO updateBooking(ReservationUpdateRequest reservationUpdateRequest);

    DefaultResponseDTO deleteABooking(ReserveDTO reserveDTO);

    Set<LocalDate> getBookingInfo();

}
