package com.alten.booking.service;


import com.alten.booking.dto.ReserveDTO;
import com.alten.booking.dto.response.ReservationResponse;
import com.alten.booking.dto.resquest.ReservationRequest;
import com.alten.booking.entity.Reservation;
import com.alten.booking.entity.Room;
import com.alten.booking.exception.BookingException;
import com.alten.booking.repository.ReservationRepository;
import com.alten.booking.repository.RoomRepository;
import com.alten.booking.service.impl.ReservationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ReservationServiceImplTest {

    @InjectMocks
    private ReservationServiceImpl service;

    @Mock
    private ReservationRepository repository;

    @Mock
    private RoomRepository roomRepository;

    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(service, "maxDaysInAdvance", 30) ;
        ReflectionTestUtils.setField(service, "maxStay", 3) ;
        ReflectionTestUtils.setField(service, "defaultRoomNumber", 402) ;
        ReflectionTestUtils.setField(service, "defaultRoomFloor", 4) ;
    }

    @Test
    public void shouldGetAvailableDates(){
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        Set<LocalDate> dateSet = new HashSet<>();
        dateSet.add(tomorrow);
        Set<LocalDate> response =  service.checkAvailableDates();

        Assertions.assertTrue(response.size() > 0);
    }

    @Test
    public void shouldBookARoomSuccessfully(){
        Set<LocalDate> bookDate = new HashSet<>();
        bookDate.add(LocalDate.now());
        bookDate.add(LocalDate.now().plusDays(1));
        bookDate.add(LocalDate.now().plusDays(2));
        ReservationRequest reservationRequest = ReservationRequest.builder().dates(bookDate).build();

        when(roomRepository.findByNumberAndFloor(any(), any())).thenReturn(getMockRoom());
        when(repository.save(any())).thenReturn(getMockReservation());

        ReservationResponse response =  service.bookARoom(reservationRequest);

        Assertions.assertNotNull(response.getId());
    }

    @Test
    public void shouldThrowMaximumStayException_BookARoom(){
        Exception exception = assertThrows(BookingException.class, () -> {
            Set<LocalDate> bookDate = new HashSet<>();
            bookDate.add(LocalDate.now());
            bookDate.add(LocalDate.now().plusDays(1));
            bookDate.add(LocalDate.now().plusDays(2));
            bookDate.add(LocalDate.now().plusDays(3));
            ReservationRequest reservationRequest = ReservationRequest.builder().dates(bookDate).build();
            service.bookARoom(reservationRequest);
        });

        String expectedMessage = "The maximum stay is 3 days";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void shouldThrowInvalidDateException_BookARoom(){
        Exception exception = assertThrows(BookingException.class, () -> {
            Set<LocalDate> bookDate = new HashSet<>();
            bookDate.add(LocalDate.now());
            bookDate.add(LocalDate.now().plusDays(1));
            bookDate.add(LocalDate.now().plusDays(31));
            ReservationRequest reservationRequest = ReservationRequest.builder().dates(bookDate).build();
            service.bookARoom(reservationRequest);
        });

        String expectedMessage = "Attention, you can select date with only 30 days in advance";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void shouldThrowAlreadyBookedException_BookARoom(){
        Exception exception = assertThrows(BookingException.class, () -> {
            Set<LocalDate> bookDate = new HashSet<>();
            bookDate.add(LocalDate.now().plusDays(1));
            ReservationRequest reservationRequest = ReservationRequest.builder().dates(bookDate).build();
            when(repository.existsByRoomAndReservationDatesIn(any(), any())).thenReturn(true);
            when(roomRepository.findByNumberAndFloor(any(), any())).thenReturn(getMockRoom());
            service.bookARoom(reservationRequest);
        });

        String expectedMessage = "The room 402 is already reserved";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }


    private Reservation getMockReservation() {
        Set<LocalDate> bookDate = new HashSet<>();
        bookDate.add(LocalDate.now());
        bookDate.add(LocalDate.now().plusDays(1));
        bookDate.add(LocalDate.now().plusDays(2));

        return Reservation.builder()
                .reservationDates(bookDate)
                .room(getMockRoom().get())
                .reservationCode(UUID.randomUUID())
                .id(1L)
                .build();
    }

    private Optional<Room> getMockRoom() {
        Room room = new Room();
        room.setFloor(4);
        room.setNumber(402);
        room.setId(1L);
        return Optional.of(room);
    }


}
