package com.alten.booking.resource;

import com.alten.booking.config.LocalDateAdapter;
import com.alten.booking.dto.resquest.ReservationRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ReservationResourceTest {

    private static final String PATH = "/reservation";

    @Autowired
    private MockMvc mockMvc;


    @Test
    @Order(3)
    void bookARoomTest() throws Exception {
        Set<LocalDate> dates = new HashSet<>();
        dates.add(LocalDate.now());
        ReservationRequest requestObj = ReservationRequest.builder().dates(dates).build();
        log.info(requestObj.toString());
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
                .create();

        String jsonRequest = gson.toJson(requestObj);

        mockMvc.perform(post(PATH)
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(1)
    void shouldReturnAvailableDatesTest() throws Exception {
        String today = LocalDate.now().toString();

        mockMvc.perform(get(PATH + "/available"))
                .andExpect(status().isOk())
                .andExpect(result -> Assertions.assertThat(
                        result.getResponse().getContentAsString())
                        .contains(today));
    }

    @Test
    @Order(2)
    public void checkVoidBookedDatesTest() throws Exception {

        mockMvc.perform(get(PATH + "/booked"))
                .andExpect(status().isOk())
                .andExpect(result -> Assertions.assertThat(
                                result.getResponse().getContentAsString())
                        .contains("[]"));
    }

    @Test
    @Order(4)
    public void checkTodayBookedDateTest() throws Exception {

        mockMvc.perform(get(PATH + "/booked"))
                .andExpect(status().isOk())
                .andExpect(result -> Assertions.assertThat(
                                result.getResponse().getContentAsString())
                        .contains("[\""+LocalDate.now()+"\"]"));
    }

}
