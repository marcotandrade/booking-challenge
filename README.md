# Booking Challenge



## Getting started

The project was developed using Java 17, Spring Boot, Spring Data JPA, Junit, Mockito

## Goal and Rules

- Room reservation API for a Hotel with CRUD operation in booking
- API will be maintained by the hotel’s IT department.
- As it’s the very last hotel, the quality of service must be 99.99 to 100% => no downtime
- For the purpose of the test, we assume the hotel has only one room available
- To give a chance to everyone to book the room, the stay can’t be longer than 3 days and
  can’t be reserved more than 30 days in advance.
- All reservations start at least the next day of booking,
- To simplify the use case, a “DAY’ in the hotel room starts from 00:00 to 23:59:59.
- Every end-user can check the room availability, place a reservation, cancel it or modify it.
- To simplify the API is insecure.


## API Requests
#### to access swagger  go to the link: http://localhost:8081/alten/swagger-ui/

###### Creating a reservation
```
curl --request POST \
--url http://localhost:8081/alten/reservation \
--header 'Content-Type: application/json' \
--data '{
"dates": [
"2022-05-20",
"2022-05-21",
"2022-05-22"
]
}'
```


###### get available dates
```
curl --request GET \
--url http://localhost:8081/alten/reservation/available
```


###### get booked dates
```
curl --request GET \
--url http://localhost:8081/alten/reservation/booked
```

###### delete a reservation
```
curl --request DELETE \
  --url http://localhost:8081/alten/reservation \
  --header 'Content-Type: application/json' \
  --data '{
	"id": "26d66e63-79f3-405e-a642-e753d644c633",
	"dates": [
		"2022-06-09",
		"2022-06-09"
	]
}
```

###### updating a reservation
```
curl --request PUT \
--url http://localhost:8081/alten/reservation \
--header 'Content-Type: application/json' \
--data '{
"id": "fe669a1f-6f9f-47c4-8e48-c3d4c80fd3a7",
"dates": [
"2022-06-10",
"2022-06-09",
"2022-12-07"
]
}'
```

## Changing business rules

As mentioned the Hotel has only the default room available for now and to configure the 
room you must inform the number and floor at the file application.properties <br>
Ex:
```
default.room.number=402
default.room.floor=4
```
but the future will be better and our application is prepared to multiple rooms.

As part of the booking rules it's not allowed to book a room 30 days in advance or 
booking for more then 3 days and it's also available for the hotel to change this
rules with just a configurations by setting the values below in the application.properties file
```
maximum.stay=3
maximum.reservation.days.advance=30
```

## Author
Marco Túlio Andrade Santos

## License
Licensed under Apache License 2.0

## Project status
#### Finished:
 - Development
 - Unit testing
 - Integration test
 - Dockerized
 - Simple jenkins pipeline

#### Next steps
* update to spring 3.1.0
